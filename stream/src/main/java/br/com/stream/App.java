package br.com.stream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Hello world!
 *
 */
public class App {

	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	public static void main(String[] args) {

		/*
		 * Lambda que ordena pelo maior IDj
		 * Multiplica os pontos em 20X
		 * e remove usuario com menos de 100 pontos
		 * 
		 * */
		exemplo1();

	}

	private static void exemplo1() {
		//List<Usuario> lambad1List = 
				populateList().stream()
				.filter(e -> e.getPontos() >= 100)
				.sorted((e1, e2) -> Integer.compare(e1.getPontos(), e2.getPontos()))
				//.forEach(System.out::println);
				//.map(e -> e.tornarModerador())
				//.reduce(accumulator)
				.collect(Collectors.toList());
		
		//System.out.println(gson.toJson(lambad1List));
	}
	
	
	private static void exemplo2() {
		//List<Usuario> lambad1List = 
				populateList().stream()
				.filter(e -> e.getPontos() >= 100)
				.sorted((e1, e2) -> Integer.compare(e1.getPontos(), e2.getPontos()))
				.forEach(System.out::println);
				//.map(e -> e.tornarModerador())
				//.reduce(accumulator)
				//.collect(Collectors.toList());
		
		//System.out.println(gson.toJson(lambad1List));
	}

	private static List<Usuario> populateList() {

		List<Usuario> usuariosList = new ArrayList<Usuario>();

		Usuario mineiro = new Usuario();
		mineiro.setNome("Rafael Tupinambá");
		mineiro.setId(1);
		mineiro.setPontos(100);

		Usuario Deleon = new Usuario();
		Deleon.setNome("Xara paralelo");
		Deleon.setId(2);
		Deleon.setPontos(75);

		Usuario xarazeira = new Usuario();
		xarazeira.setNome("Xara original");
		xarazeira.setId(3);
		xarazeira.setPontos(500);

		Usuario jhon = new Usuario();
		jhon.setNome("Jhon");
		jhon.setId(4);
		jhon.setPontos(2500);

		Usuario lulu = new Usuario();
		lulu.setNome("Lulu");
		lulu.setId(5);
		lulu.setPontos(40);

		usuariosList.add(mineiro);
		usuariosList.add(Deleon);
		usuariosList.add(xarazeira);
		usuariosList.add(lulu);
		usuariosList.add(jhon);

		return usuariosList;
	}

}
