package br.com.stream;

public class Usuario {
	
	private int id;
	private String nome;
	private int pontos;
	private boolean moderador;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getPontos() {
		return pontos;
	}
	public void setPontos(int pontos) {
		this.pontos = pontos;
	}
	public boolean isModerador() {
		return moderador;
	}
	public void setModerador(boolean moderador) {
		this.moderador = moderador;
	}
	
	public void multiplica() {
		setPontos(getPontos()*20);
	}
	
	public void tornarModerador() {setModerador(true);}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getNome();
	}
	
}
